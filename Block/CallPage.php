<?php


namespace Kowal\CallPageIOWidget\Block;

class CallPage extends \Magento\Framework\View\Element\Template
{

    protected $scopeConfig;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [],
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        parent::__construct($context, $data);
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return string
     */
    public function Widget()
    {

        //Your block code
        return $this->getCode();
    }

    private function getCode()
    {
        return $this->scopeConfig->getValue('callpagewidget/script/code', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

}
